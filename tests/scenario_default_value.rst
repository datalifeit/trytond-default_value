=======================
Default value Scenario
=======================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from proteus import Model, Wizard, Report
    >>> from trytond.tests.tools import activate_modules

Install default value::

    >>> config = activate_modules('default_value')

Get current user::

    >>> User = Model.get('res.user')
    >>> user = User(config.user)

Get admin group::

    >>> Group = Model.get('res.group')
    >>> admin_group, = Group.find([
    ...         ('name', '=', 'Administration'),
    ...         ])

Create a new user::

    >>> new_user = User()
    >>> not new_user.signature
    True

Create a default value for user model::

    >>> Defvalue = Model.get('default.value')
    >>> IrModel = Model.get('ir.model')
    >>> IrField = Model.get('ir.model.field')
    >>> user_model, = IrModel.find([('model', '=', 'res.user')])
    >>> user_field, = IrField.find([
    ...     ('model', '=', user_model.id),
    ...     ('name', '=', 'signature')])
    >>> defvalue = Defvalue()
    >>> defvalue.model = user_model
    >>> defvalue.field = user_field
    >>> defvalue.field_type == user_field.ttype
    True
    >>> defvalue.user = user
    >>> defvalue.text = 'Regards'
    >>> defvalue.save()
    >>> defvalue.default_value
    'Regards'

Create a new user and check default signature::

    >>> new_user = User()
    >>> new_user.signature
    'Regards'

Check constraint and ordering::

    >>> defvalue = Defvalue()
    >>> defvalue.model = user_model
    >>> defvalue.field = user_field
    >>> defvalue.field_type == user_field.ttype
    True
    >>> defvalue.user = user
    >>> defvalue.text = 'Best regards'
    >>> defvalue.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelsql.SQLConstraintError: You can not define more than one default value for the same model, field and user. - 
    >>> defvalue.user = None
    >>> defvalue.save()

Create a new user again::

    >>> new_user = User()
    >>> new_user.signature
    'Regards'
    >>> new_user.name = 'A new user'
    >>> new_user.login = 'newuser'
    >>> new_user.groups.append(admin_group)
    >>> new_user.save()

Change user and check default value::

    >>> config.user = new_user.id
    >>> new_user = User()
    >>> new_user.signature
    'Best regards'

Test many2one default value::

    >>> Language = Model.get('ir.lang')
    >>> lang, = Language.find(['translatable', '=', True], limit=1)
    >>> user_field, = IrField.find([
    ...     ('model', '=', user_model.id),
    ...     ('name', '=', 'language')])
    >>> defvalue.model = user_model
    >>> defvalue.field = user_field
    >>> defvalue.many2one = str(lang.id)
    >>> defvalue.save()

    >>> new_user = User()
     >>> new_user.name = 'Another new user'
    >>> new_user.login = 'newuser2'
    >>> new_user.language.id == lang.id
    True
    >>> new_user.save()